# Installing Drupal 8 with composer and Docker - docker4drupal
A full walkthrough to install Drupal 8 localy with docker and composer

## commands
```
mkdir Sites
cd Sites\
composer create-project drupal-composer/drupal-project:8.x-dev docker_drupal --no-interaction
git clone https://github.com/wodby/docker4drupal.git docker_drupal_server 
rm -R drupal_docker_server/.git 
rm -R docker_drupal_server/docker-compose.override.yml
cp -Rf docker_drupal_server/ docker_drupal/ 
cd docker_drupal 
docker-compose up -d
docker ps -a
choco install make
make help
make shell
drush --version
drush help
drush sql-dump > ./mariadb-init/dump.sql
drush sql-drop
drush sql-cli < ./mariadb-init/dump.sql
exit
make down
```
If all goes well, you can find your brand new Drupal 8 site on [drupal.docker.localhost:8000](http://drupal.docker.localhost:8000).<br>
<small>And [here](http://portainer.drupal.docker.localhost:8000) you should find portainer.</small>


## install your Drupal 8
```
# .env
DB_NAME=drupal
DB_USER=drupal
DB_PASSWORD=drupal
DB_ROOT_PASSWORD=password
DB_HOST=mariadb
DB_DRIVER=mysql
```




### sources:
* https://www.youtube.com/watch?v=aYb8C18HjmY
* ~~https://www.youtube.com/watch?v=m2U5RjlocF0~~ (old)
* https://github.com/drupal-composer/drupal-project
* https://github.com/wodby/docker4drupal
